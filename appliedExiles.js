const MongoClient = require('mongodb').MongoClient;
const config = require("./config.json");
const escape = require('markdown-escape');
const appliedExilesCollection = "appliedExiles";
const index = require('./index');
const Sentry = require('@sentry/node');
const appliedRole = '521284023352557568';

var appliedExilesModule = module.exports = {

    pendingInvites: function (receivedMessage) {
        MongoClient.connect(config.exileDbUrl, {
            useNewUrlParser: true
        }).then(client => {
            dbclient = client.db(config.exileDbName);
            dbclient.collection(appliedExilesCollection).find({}).toArray(function (err, result) {
                if (err) return console.log(err);

                if (!result) {
                    receivedMessage.channel.send(`There are no pending guild invites to action.`);
                } else {
                    receivedMessage.channel.send(`There are currently ${result.length} pending guild invites.`);
     
                    result.forEach(e => {
                        if (e.latestCharacter) {
                            receivedMessage.channel.send( `Discord User: <@${e.discordUid}>, PoE Account: ${escape(e.ignAccountName)}, IGN: ${e.latestCharacter.name} - Applied Date: ${e.applyDt}`);
                        } else {
                            receivedMessage.channel.send( `Discord User: <@${e.discordUid}>, PoE Account: ${escape(e.ignAccountName)}, Private Profile - Applied Date: ${e.applyDt}`);
                        }
                    });
                }
            });
        }).catch(function (err) {
            console.log(err)
        });
    },
    removeUser: function (discordUid, receivedMessage) {
        let discordUidRegex = /^\d+$/;
        if (!discordUidRegex.test(discordUid)) {
            receivedMessage.channel.send('Invalid discord snowflake provided should be only numeric.');
            return;
        }

        MongoClient.connect(config.exileDbUrl, {
            useNewUrlParser: true
        }).then(client => {
            dbclient = client.db(config.exileDbName);
            dbclient.collection(appliedExilesCollection).deleteOne({ "discordUid": discordUid }, function (err, result) {
                if (err) return console.log(`Error deleting applied exile Uid - ${discordUid} - ${err}`);
                
                if (result.deletedCount === 1) {
                    receivedMessage.channel.send(`The users application has been removed.`);
                    let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);
                    discordGuild.fetchMember(discordUid).then(guildMember => {
                        guildMember.removeRole(appliedRole);
                    }).catch(function (err) {
                        console.log(`Error fetching guild member ${discordUid} to remove Applied Role - ${err}`);
                    });
                } else {
                    receivedMessage.channel.send(`The users application has was not found.`);
                }
            });
        }).catch(function (err) {
            console.log(err)
        });
    }


}