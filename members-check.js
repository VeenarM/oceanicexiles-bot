const index = require('./index');
const cheerio = require('cheerio');
const MongoClient = require('mongodb').MongoClient;
const config = require("./config.json");
const userOnlineStatRunner = require("./userOnlineStats");
const escape = require('markdown-escape');
const moment = require('moment-timezone');
const SORT = require('fast-sort');
require('events').EventEmitter.defaultMaxListeners = 60;
const igExilesCollection = "igExiles";
const appliedMembersCollection = "appliedExiles";
const discordAuthorRex = new RegExp(/^<{1}@{1}!?\d+>{1}$/);
const authenticate = require("./authenticate");
const request = authenticate.request;
const Sentry = require('@sentry/node');
const officerRole = '125818927325380608';
const appliedRole = '521284023352557568';
const unverifiedRole = '519771288345247744';
const exMemberRole = '419998937601146880';
const memberRole = '125822121468362752';

const challengesTop10Role = '521418374497042441';
const challengesTop25Role = '521418670900248606';
const challengesTop50Role = '521418766899478538';



var currentMemberLength = [];

var membersModule = module.exports = {

    currentMemberLength: currentMemberLength,

    whoisDiscordUser: function (args, receivedMessage) {
        var author = args[1];
        if (!discordAuthorRex.test(author)) {
            receivedMessage.channel.send('Invalid syntax received, not a valid discord author.');
            return;
        }

        author = author.replace(/[^0-9]/g, '').trim();

        try {
            MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
                dbclient = client.db(config.exileDbName);
                dbclient.collection(igExilesCollection).findOne({ discordUid: author }, function (err, result) {
                    if (err) return console.log(err);

                    if (!result) {
                        receivedMessage.channel.send(`No discord -> IGN link found.`);
                    } else {
                        var charStr = 'No Characters Found - likely profile is private.'
                        if (result.characters && result.characters.length > 0) {
                            charStr = `**${config.currentLeague} League Characters:** `;
                            for (var i = 0; i < result.characters.length; i++) {
                                let c = result.characters[i];
                                if (c.league === "Betrayal") {
                                    charStr += `Name: ${c.name}, Class: ${c.class}, Level: ${c.level}, Experience: ${c.experience}`;
                                }
                            }
                        }
                        receivedMessage.channel.send(`**IGN:** ${escape(result.id)}, **Last Online:** ${result.last_online}, **Linked At:** ${result.linkDate}, ${charStr}`);
                    }
                });
            }).catch(function (err) {
                console.log(err)
            });
        } catch (error) {
            console.log(`Error attempting to locate uid of ${JSON.stringify(author)} - user is unlikely linked there account.`);
        }
    },
    retrieveCurrentGuildMembers: function (receivedMessage) {
        MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
            dbclient = client.db(config.exileDbName);
            dbclient.collection(igExilesCollection).find({}).toArray(function (err, result) {
                if (err) return console.log(err);
                receivedMessage.channel.send('We currently have ' + result.length + ' members.');
            });
        }).catch(function (err) {
            console.log(err)
        });
    },

    scrapeGuildMembers: function () {
        request('https://www.pathofexile.com/guild/profile/7022', { json: true }, (err, res, body) => {
            if (err) {
                console.log(`Error loading POE Guild Website, It's likely offline - setting retry attempt.`);
                setNextTimeOutPeriod()
                return console.log(err);
            }

            let $ = cheerio.load(body);
            let igExileNames = [];

            $('div.members div.member').each(function (idx, element) {
                let $profile = $(element).find('span.profile-link a');
                let $lastOnline = $(element).find('span').data('time');
                let $memberType = $(element).find('span.memberType');
                // We only want to do this if we're logged in properly...
                if ($lastOnline) {
                    $lastOnline = $lastOnline.replace('Z', '');
                    let currentTz = moment.tz($lastOnline, 'Australia/Sydney');
                    let dateAsUtc = currentTz ? new Date(currentTz.utc()) : null;

                    let $challengesCompletedText = $(element).find('img.achievement').attr('title');
                    let completedChallenges = 0;
                    if ($challengesCompletedText) {
                        try {
                            completedChallenges = parseInt($challengesCompletedText.substr(10, 2).trim());
                        } catch (err) {
                            console.log('error in parsing int string, likely its undefined.');
                        }
                    }
    
                    let exile = { id: $profile.text(), last_online: dateAsUtc, challengesCompleted: completedChallenges, memberType: $memberType.text() };
                    igExileNames.push(exile);
                }
            });

            currentMemberLength = igExileNames.length;

            // If no exiles are found it means that there's an error with the website.
            if (igExileNames.length > 0) {
                console.log(`### UPDATING IN GAME EXILES ROUTINE AND ONLINE STATS AT - ${new Date()} ####`);
                updateInGameExiles(igExileNames);
                userOnlineStatRunner.runUserOnlinStatAnalysis(igExileNames);
            } else {
                console.log(`POE Webiste may be down, unable to scrape/update guild members/stats, maybe authentication?`);
            }

            // Reset next Cron to run this update job
            setNextTimeOutPeriod();
        });
    },

    retrieveGuildChallengeLadder: function (args, receivedMessage) {
        let limit = 15;
        
        // TODO should we allow more to be entered?
        MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
            db = client.db(config.exileDbName);
            db.collection(igExilesCollection).find({ discordUid: { $exists: true } }).sort({ challengesCompleted : -1 }).toArray(function (err, result) {
                if (err) return console.log(err);
                // All records that are linked returned

                let linkedExiles = result;

                let topLimit = linkedExiles.slice(0, limit);
                topLimit = incrementBasedOnArrayPos(topLimit, linkedExiles, topLimit[topLimit.length - 1], linkedExiles[topLimit.length]);

                let challengeOutput = '';
                let outputLines = 1; // Embed links can only have 15 lines of text...
                let userRank = 1; // Sets the output value, e.g. if 2 people have the same challenges, they're the same rank...
                let lastRecord = false;
                topLimit.forEach(e => {
                    if (outputLines <= 15 && challengeOutput.length < 900) {
                        if (lastRecord && lastRecord.challengesCompleted !== e.challengesCompleted) {
                            userRank++;
                        }

                        challengeOutput += `${userRank}) Account: ${escape(e.id)}, Discord: ${escape(e.discordUsername)}, Completed Challenges: ${e.challengesCompleted}\n`;
                        outputLines++;
                        lastRecord = e;                        
                    }
                });
 
                receivedMessage.channel.send({
                    embed: {
                        "color": 12488439,
                        "fields": [
                            {
                                "name": "Oceanic Exile League Challenge Rankings",
                                "value": challengeOutput,
                            }
                        ]
                    }
                });

                if (outputLines > 15 || challengeOutput.length > 900) {
                    receivedMessage.channel.send(`Embed feature has a limit of 1024 characters, unable to list the others who're equal ${ordinal_suffix_of(userRank)}`);
                }
            });
        }).catch(function(err) {
            console.log(err);
        });
    },


    retrieveGuildMembersRankingsLadder: function (args, receivedMessage) {
        let limit = 10;
        
        // TODO should we allow more to be entered?
        MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
            db = client.db(config.exileDbName);
            db.collection(igExilesCollection).find({ discordUid: { $exists: true }, characters : { $gt: [] }}).toArray(function (err, result) {
                if (err) return console.log(err);

                // We're not allowing non-linked users to participate... (Even though the data is available)
                console.log(`${result.length} - result length`);
                let betrayalCharacterList = result.filter(o => o.characters.find(f => f.league === config.currentLeague));

                console.log(`${betrayalCharacterList.length} - betrayalCharacterList length`);
                let linkedExiles = betrayalCharacterList;

                // Sort the characters first (filter out non-league chars.)
                linkedExiles.forEach(c => {
                    c.characters = c.characters.filter(c => c.league === config.currentLeague);
                    SORT(c.characters).by([
                        { desc : 'level'},
                        { desc : 'experience'},
                        { asc :  'hundredReachedAt' }
                    ]);
                })

                // Now sort the exiles by there highest character with earlier achieved hundred date.
                SORT(linkedExiles).by([
                    { desc: c => c.characters[0].level },
                    { desc: c => c.characters[0].experience },
                    { asc: c => c.characters[0].hundredReachedAt },
                ]);

                let topLimit = linkedExiles.slice(0, limit);

                var challengeOutput = `\`\`\`css\n[Oceanic Exile Guild Ladder XP Rankings for ${config.currentLeague} League]\n`;
                var posIdx = 1;
                topLimit.forEach(e => {
                    var char = e.characters[0];
                    challengeOutput += `${posIdx++}. Account.${e.id} Character.${char.name} Level.${char.level} Experience.${char.experience} Class.${char.class} ${char.level == 100 ? 'ReachedAt.' + moment.tz(char.hundredReachedAt, 'Australia/Sydney').format('DD-MM-YYYY hh:mmA') : ''}\n`;
                });
                challengeOutput+= '```';
                receivedMessage.channel.send(challengeOutput);
            });
        }).catch(function(err) {
            console.log(err);
        });
    }
};


//TODO - should we set this to be a time period based on when it was executed? 
//e.g - every 10 minutes or set time to next 10min minus 1, for those who login in the potential 11:50PM -> midnight window?
function setNextTimeOutPeriod() {
    setTimeout(membersModule.scrapeGuildMembers, config.refreshUsersTtl);
};

var completed_requests = 0;

async function getExileCharacterDetails(igExiles) {
    completed_requests = 0;
    return new Promise(function(resolve, reject) {
        for (var i = 0; i < igExiles.length; i++) {
            setTimeout(executeGetCharacterCall, 200 + ( i * 300 ), igExiles[i], igExiles, resolve);
        };
    });
}


function executeGetCharacterCall(e, igExiles, resolve) {
    var encodedUrl = encodeURI('https://www.pathofexile.com/account/view-profile/' + e.id + '/characters');

    request({ url : encodedUrl, json : true}, (err, res, body) => {
        completed_requests++;
        if (err) {
            console.log(`Error when looping character info for id ${e.id}`)
        } else {
            try {
                var isNotPrivate = body.indexOf('This profile tab has been set to private') === -1;
                if (isNotPrivate) { // Profile is not private get character details.
                    var foundIdx = body.indexOf('new C({"name"');
                    if (foundIdx === -1) {
                        throw new Error(`Object for Name not found in body response object - likely weird fkn characters in there name at the moment not resolving....`);
                    } else {
                        var str = body.substr(foundIdx + 6);
                        var fullJsonString = str.substr(0, str.indexOf('}') + 1);
                        let characterObject = JSON.parse(fullJsonString);
                        e.character = characterObject;
                    }
                }
            } catch (e1) {
                console.log(`${e.id} - with URL(${encodedUrl}) - ${e1}`);
            }
        }

        if (completed_requests >= igExiles.length) {
            console.log(`Resolving Requests as completed requests now completed @ ${new Date()}`);
            resolve(igExiles);
        }
    });
}

// List of users from website as param.
function updateInGameExiles(inGameExiles) {
    getExileCharacterDetails(inGameExiles).then(function(igExiles) {
        MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
            db = client.db(config.exileDbName);
            db.collection(igExilesCollection).find({}).toArray(function (err, result) {
                if (err) return console.log(err);
    
                // No records, insert all records.(only run on first instance of bot setup).
                if (result.length == 0) {
                    db.collection(igExilesCollection).insertMany(igExiles, function (err, res) {
                        if (err) return console.log(err);
                        console.log(igExiles.length + ' records inserted');
                    });
                    return;
                }
    
                // List of guild members received from the website. (LATEST)
                let igExileUsers = igExiles.map(val => val.id);
    
                // List of guild members current in our database. (CURRENT)
                let databaseExiles = result.map(val => val.id);
    
                // Exiles who aren't in the database!! (New users!!)
                let newExiles = igExileUsers.filter(val => !databaseExiles.includes(val));
                console.log(`NEW EXILES: (${newExiles.length}) : ${newExiles}`);
    
                // Exiles who're in the database but not in the guild now (Exiled users)
                let exiledExiles = databaseExiles.filter(val => !igExileUsers.includes(val));
                console.log(`EXILED EXILES: (${exiledExiles.length}) : ${exiledExiles}`);
    
                let currentExiles = igExiles.filter(val => databaseExiles.includes(val.id));
                console.log(`CURRENT EXILES: (${currentExiles.length}) `);
    
                if (newExiles.length > 0) {
                    let newExiledObj = newExiles.map(m => ({ id: m }))
                    // NEW EXILES - > Save strait to the new list, then attempt to find any links in the temp table and modify after.
                    db.collection(igExilesCollection).insertMany(newExiledObj, (err, res) => {
                        if (err) return console.log(`error inserting new exilesr`);
                        notifyDiscordChannel(newExiles, config.membersLogChannel, true);
                        linkRecords(newExiles, client);
    
                        if (exiledExiles.length > 0) {
                            removeExilesFromDatabase(exiledExiles, db, igExileUsers, result);
                            notifyDiscordChannel(exiledExiles, config.membersLogChannel, false);
                        }
                    });
                } else {
                    if (exiledExiles.length > 0) {
                        removeExilesFromDatabase(exiledExiles, db, igExileUsers, result);
                        notifyDiscordChannel(exiledExiles, config.membersLogChannel, false);
                    }
                }
    
                // Assuming the calls all went well, update discord roles & database char data
                if (currentExiles.length > 0) {
                    updateLastOnlineTimeAndChallenges(currentExiles, db).then(function () {
                        updateCharactersForUsers(currentExiles, result, db);
                        updateDiscordChallengeRoles(result);
                    });
                }
            });
        }).catch(function (err) {
            console.log(err)
        });
    }, function(err) {
        console.log(err);
    })
}

const init10 = 10;

// Only does it linked users, we only assess linked users.
function updateDiscordChallengeRoles(dbExiles) {
    let linkedExiles = dbExiles.filter(val => (val.discordUid));
    
    linkedExiles.sort((a, b) => b.challengesCompleted - a.challengesCompleted);

    let sliceTop10 = linkedExiles.slice(0, init10);
    sliceTop10 = incrementBasedOnArrayPos(sliceTop10, linkedExiles, sliceTop10[sliceTop10.length - 1], linkedExiles[sliceTop10.length]);

    let sliceTop25 = linkedExiles.slice(sliceTop10.length, sliceTop10.length + 15);
    sliceTop25 = incrementBasedOnArrayPos(sliceTop25, linkedExiles, sliceTop25[sliceTop25.length - 1], linkedExiles[sliceTop25.length]);

    let sliceTop50 = linkedExiles.slice(sliceTop25.length, sliceTop25.length + 25);
    sliceTop50 = incrementBasedOnArrayPos(sliceTop50, linkedExiles, sliceTop50[sliceTop50.length - 1], linkedExiles[sliceTop50.length]);

    // Role adjustments now to occur.
    let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);

    // Remove all the challenge roles from the rest of the users.

    linkedExiles.forEach(e => {
        discordGuild.fetchMember(e.discordUid).then(guildMember => {
            var modifyRolePromise = new Promise(function(resolve, error) {
                let addRole;
                let removeRoles = [challengesTop10Role, challengesTop25Role, challengesTop50Role]
                if (sliceTop10.indexOf(e) !== -1) {
                    addRole = challengesTop10Role;
                    removeRoles.splice(0, 1);
                } else if (sliceTop25.indexOf(e) !== -1) {
                    addRole = challengesTop25Role;
                    removeRoles.splice(1, 1);
                } else if (sliceTop50.indexOf(e) !== -1) {
                    addRole = challengesTop50Role;
                    removeRoles.splice(2, 1);
                }

                if (addRole) {
                    guildMember.addRole(addRole).then(gm => {
                        guildMember.removeRoles(removeRoles).catch(function (err) {
                            console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to remove roles ${removeRoles} -  ${err}`);
                        }).then(function() {
                            resolve();
                        });
                    }).catch(function (err) {
                        console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to add role(${addRole}), likely now skipped the removal of challenge roles -  ${err}`);
                        resolve();
                    });
                } else {
                    guildMember.removeRoles(removeRoles).catch(function (err) {
                        console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Error removing roles ${removeRoles} -  ${err}`);
                    }).then(function() {
                        resolve();
                    });
                }
            });

            // Manage Officer Roles... 
            modifyRolePromise.then(function() {
                let hasOfficerRoleDiscord = guildMember.roles.has(officerRole);
                if ((e.memberType === 'Officer' || e.memberType === 'Leader') && !hasOfficerRoleDiscord) {
                    console.log(`Officer Detected, with no discord role - need to add it for user ${e.discordUsername}`);
                    guildMember.addRole(officerRole).catch(function (err) {
                        console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to add officer role - ${err}`)
                    });
                }

                if (e.memberType === 'Member' && hasOfficerRoleDiscord) {
                    console.log(`Member Detected, with officer discord role - need to remove it from user ${e.discordUsername}`);
                    guildMember.removeRole(officerRole).catch(function (err) {
                        console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to remove officer role - ${err}`)
                    });
                }
            }).catch(function (err) {
                console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Error in role promise for officer adjustment. ${removeRoles} -  ${err}`);
            });
        }).catch(function (err) {
            // Error in fetching discord member - likely user not found in guild.
            let channel = index.dclient.channels.get(config.membersLogChannel);
            if (channel && -1 === errorFetchedForUserIgnoreList.indexOf(e.discordUid)) {
                channel.send(`updateDiscordChallengeRoles::Failed to fetchMember ${e.discordUsername} with UID: ${e.discordUid} - ${err}`);
                console.log(`updateDiscordChallengeRoles::Failed to fetchMember ${e.discordUsername} with UID: ${e.discordUid} - ${err}`);
                errorFetchedForUserIgnoreList.push(e.discordUid);
            }
        });
    });
}

var errorFetchedForUserIgnoreList = [];

/**
 * Loops through the indexes to add any 'same' values as the n(th) index.
 * @param {} topArray 
 * @param {*} linkedExiles 
 * @param {*} lastRecord 
 * @param {*} nextRecord 
 */
function incrementBasedOnArrayPos(topArray, linkedExiles, lastRecord, nextRecord) {
    var nextRecord = nextRecord;
    let idx = topArray.length;
    while (lastRecord.challengesCompleted === nextRecord.challengesCompleted) {
        nextRecord = linkedExiles[idx++];
        if (nextRecord.challengesCompleted === lastRecord.challengesCompleted) {
            topArray.push(nextRecord);
        }
    }
    return topArray;
}

async function updateLastOnlineTimeAndChallenges(currentExiles, db) {
    return new Promise(function(resolve, error) {
        var bulkArray = [];
        currentExiles.forEach(e => {
            if (e.last_online != null) {
                bulkArray.push({
                    updateOne: {
                        filter: { id: e.id },
                        update: { $set: { last_online : e.last_online, challengesCompleted: e.challengesCompleted, memberType: e.memberType } }
                    }
                });
            }
        });
        if (bulkArray.length > 0) {
            db.collection(igExilesCollection).bulkWrite(bulkArray);
        }
        return resolve();
    });
};

async function updateCharactersForUsers(currentExiles, databaseExiles,  db) {
    return new Promise(function(resolve, error) {
        let bulkArray = [];

        currentExiles.forEach(e => {
            if (e.character != null && e.character != undefined) {
                let characters = [];
                
                // Get list of current characters from DB, append all (except the character with the same name)
                let dbExile = databaseExiles.find(o => o.id === e.id);
                if (dbExile.characters != undefined && dbExile.characters != null && dbExile.characters.length > 0) {
                    dbExile.characters.forEach(dbChar => {
                        if (dbChar.name !== e.character.name) {
                            characters.push(dbChar);
                        } else {
                            e.character.hundredReachedAt = dbChar.hundredReachedAt;
                        }
                    });
                }

                // Add this character we pulled to the list.
                characters.push(e.character);

                // This saves the first registered time the user reached level 100.
                characters.forEach(c => {
                    if (c.level === 100 && !c.hundredReachedAt) {
                        c.hundredReachedAt = new Date(moment.tz('Australia/Sydney').utc());
                        console.log(`# Setting date to NOW for newly aquired level 100 character - ${JSON.stringify(c)}`);
                    }
                });

                bulkArray.push({
                    updateOne: {
                        filter: { id: e.id },
                        update: { $set: { characters : characters } }
                    }
                });
            }
        });

        if (bulkArray.length > 0) {
            db.collection(igExilesCollection).bulkWrite(bulkArray);
        }
        return resolve();
    });
};


function linkRecords(recordsToLink, client) {
    dbclient = client.db(config.exileDbName);

    recordsToLink.forEach(e => {
        dbclient.collection(appliedMembersCollection).findOne({ "ignAccountName": { $regex: new RegExp(e, "i") } }, function (err, result) {
            if (err) return console.log('Error finding user for linking = ' + e + '\n ' + err);
            if (!result) {
                console.log('User has no discord linking info (may of been added with out going through acceptance process');
            } else {
                // Now link the accounts...
                var newvalues = { $set: { "discordUid": result.discordUid, "discordUsername": result.discordUsername, "linkDate": new Date() } };
                dbclient.collection(igExilesCollection).updateOne({ "id": e }, newvalues, function (err, res) {
                    if (err) return console.log(`Error linking ${e} failed to update record: \n ${err}`);
                    console.log(`${e} document from ${igExilesCollection} updated`);

                    addNewMemberRoles(result.discordUid);

                    dbclient.collection(appliedMembersCollection).deleteOne({ "ignAccountName": { $regex: new RegExp(e, "i") } }, function (err, obj) {
                        if (err) return console.log('Error deleting linked account ' + e + '\n ' + err);
                        console.log(`${e} document from ${appliedMembersCollection} deleted`);
                    });
                });
            }
        });
    });
};

// true / false  -determines if new users or external users
// @Params <list> <discordChannelId> <true/false>
function notifyDiscordChannel(exiles, channelId, newFlag) {
    if (exiles.length < 1) {
        return;
    }

    var exilesStringed = '';
    exiles.forEach(e => {
        exilesStringed = exilesStringed + (newFlag ? '+ ' : '- ') + e + '\n';
    });

    let message = '```diff\n' + exilesStringed + '```';

    index.dclient.channels.get(channelId).send(message);
}

function removeExilesFromDatabase(exiledExiles, db, igExileUsers, result) {
    let exiledExilesdObj = exiledExiles.map(m => ({ id: m }))
    for (var i = 0; i < exiledExilesdObj.length; i++) {
        db.collection(igExilesCollection).deleteOne(exiledExilesdObj[i], function (err, obj) {
            if (err) throw err;
            if (i === exiledExilesdObj.length) {
                removeExilesAdjustRoles(result, igExileUsers);
            }
        });
    }
}


// REMOVE MEMBERS - > USER DISCORD ROLES (Remove 'Member', 'Challenge roles', officerRole -> Add  'Ex Member')
function removeExilesAdjustRoles(result, currentExileSiteList) {
    var exiledExiles = result.filter(val => !currentExileSiteList.includes(val.id));
    exiledExiles.forEach(e => {
        if (e.discordUid) {
            let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);
            discordGuild.fetchMember(e.discordUid).then(guildMember => {
                guildMember.addRole(exMemberRole).then(gm => {
                    gm.removeRoles([memberRole, challengesTop10Role, challengesTop25Role, challengesTop50Role, officerRole]).catch(function (err) {
                        console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to remove roles in removeExileRoles ${err}`)
                    });
                }).catch(function (err) {
                    console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to add role ex member ${err}`)
                });
            }).catch(function (err) {
                console.log(`Discord User (${e.discordUsername}) - ${e.discordUid} - Failed to fetch member ${err}`)
            });
        } else {
            console.log(`${e.id} - User was not linked, no action`);
        }
    })
}


// ADD MEMBERS - > USER DISCORD ROLES (Remove 'Ex Member, Unverified', 'Applied' -> Add  'Member')
function addNewMemberRoles(discordUid) {
    if (discordUid) {
        let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);
        discordGuild.fetchMember(discordUid).then(guildMember => {
            guildMember.addRole(memberRole).then(gm => {
                guildMember.removeRoles([exMemberRole, unverifiedRole, appliedRole]).catch(function (err) {
                    console.log(`Discord User (${discordUid}) - Failed to remove roles in addNewMember roles ${err}`)
                });
            }).catch(function (err) {
                console.log(`Discord User (${discordUid}) - Failed to add role member ${err}`)
            });
        }).catch(function (err) {
            console.log(`Discord User (${discordUid}) -  Failed to add fetch roles in addNewMemberroles ${err}`)
        });
    } else {
        console.log(`Members UID not found - ${discordUid}`);
    }
}

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}