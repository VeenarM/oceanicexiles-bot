// Execute the authenticate module to load request jar with cookies.
const config = require("./config.json");
const Sentry = require('@sentry/node');
Sentry.init(config.SentryOpts);

require("./authenticate.js");
const Discord = require("discord.js");
const botAuthConfig = require('./bot-auth.json');
const commands = require('./commands');
const botMembersService = require('./members-check');
const client = new Discord.Client();
const MongoClient = require('mongodb').MongoClient;

module.exports.dclient = client;

client.on("ready", function () {
    console.log(`Ready as ${client.user.username}`);

    // This is just for a nice way to view access channels
    client.guilds.forEach((guild) => {
        console.log("################################################");
        console.log("Guild -- " + guild.name + ' - ' + guild.id);
        // List all channels
        guild.channels.forEach((channel) => {
            console.log(`Channel -- ${channel.name} (${channel.type}) - ${channel.id}`)
        })
        console.log("########### ROLES ########################");
        guild.roles.forEach((role) => {
            console.log(`Role ID -- ${role.id} (${role.name})`)
        })
    })

    Sentry.captureMessage(`## Initialization of Oceanic Exiles Completed ##`);

    client.on('message', (receivedMessage) => {
        // Prevent bot from responding to its own messages
        if (receivedMessage.author == client.user) {
            return
        }

        if (receivedMessage.content.startsWith("!")) {
            if (receivedMessage.channel.type === 'dm' || receivedMessage.channel.id === config.botCommandsChannel || receivedMessage.channel.id === config.botAdminCommandsChannel || receivedMessage.channel.id === config.leagueChatChannel) {
                commands.processCommand(receivedMessage);
            } else {
                receivedMessage.channel.send(`I only listen to commands sent directly to me or in the <#${config.botCommandsChannel}> channel.`);
            }
        }
    })

    // Execute data checks - pull latest lists, Setup Cron Timers for data mining
    botMembersService.scrapeGuildMembers();

});

client.on("guildMemberAdd", (member) => {
    // Check if user is already a member, and/or welcome back when we don't remove chars?
    member.send(`Welcome to Oceanic Exiles, if you're interested in joining please go to #rules-recruitment for details on how to join.`);
});

client.on("guildMemberRemove", (member) => {
    if (member.roles.has(config.roles.memberRole)) {
        MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(dbclient => {
            dbclient.db(config.exileDbName).collection(config.igExilesCollection).findOne({ discordUid: member.id }, function (err, result) {
                if (result) {
                    client.channels.get(config.membersLogChannel).send(`<@${config.guildOwnerUid}> Discord User: ${member} left the discord server need to be removed from guild in game. Account Name: ${result.id}`);
                } else {
                    client.channels.get(config.membersLogChannel).send(`<@${config.guildOwnerUid}> Discord User: ${member} left the discord server (Can't find the linked IGN, may of already left? or was never linked.)`);
                }
            });
        }).catch(err => {
            Sentry.captureException(err);
        });
    } else {
        client.channels.get(config.membersLogChannel).send(`Discord User: ${member} has left the discord server. (No action required).`);
    }
});


client.login(botAuthConfig.discordToken);

// Run wiki-bot
const wikiBot = require('./wiki-bot');

// Only works on Windows (or a system with a running game client), could be invoked individually.
if (config.enableGuildChatLogging) {
    const guildChat = require('./guild-chat');
}

const newMemberApp = require('./new-member');