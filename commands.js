const botPatches = require('./patches');
const botPoeApiServices = require('./poe-api-services');
const botMembersService = require('./members-check');
const appliedExilesServices = require('./appliedExiles');
const newMemberService = require('./new-member');
const userOnlineStatRunner = require("./userOnlineStats");
const adminService = require('./admin');
const config = require("./config.json");
const Sentry = require('@sentry/node');

module.exports = {
    processCommand: function (receivedMessage) {
        let fullCommand = receivedMessage.content.substr(1) // Remove the leading exclamation mark
        let splitCommand = fullCommand.split(" ") // Split the message up in to pieces for each space
        let primaryCommand = splitCommand[0] // The first word directly after the exclamation is the command
        let arguments = splitCommand.slice(1) // All other words are arguments/parameters/options for the command

        console.log(`${receivedMessage.author.username} entered command: ${primaryCommand}, Arguments: ${fullCommand.substr(fullCommand.indexOf(" "))}  @ ${new Date()}`);
        Sentry.captureMessage(`BOT Command Received [${primaryCommand}]`, Sentry.Severity.Info);
        
        if (receivedMessage.channel.id === config.leagueChatChannel && primaryCommand !== "admin") {
            if(primaryCommand === "league") {
                botPoeApiServices.printGuildLadder(receivedMessage);
            } else {
                receivedMessage.channel.send(`I only listen to commands sent directly to me or in the <#${config.botCommandsChannel}> channel.`);
            }
            return;
        }

    
        if (primaryCommand === "help") {
            helpCommand(arguments, receivedMessage)
        } else if (primaryCommand === "rank") {
            botPoeApiServices.checkPoeLeague(arguments, receivedMessage);
        } else if (primaryCommand === "ladder") {
            botMembersService.retrieveGuildChallengeLadder(arguments, receivedMessage);
        } else if (primaryCommand === "gladder") {
            botMembersService.retrieveGuildMembersRankingsLadder(arguments,receivedMessage);
        } else if (primaryCommand === "patch") {
            botPatches.getLatestPatch(receivedMessage)
        } else if (primaryCommand === "members") {
            botMembersService.retrieveCurrentGuildMembers(receivedMessage);
        } else if (primaryCommand === "echo") {
            receivedMessage.channel.send(fullCommand.substr(fullCommand.indexOf(" ")));
        } else if (primaryCommand === "join") {
            newMemberService.joinAcceptGuildRules(arguments, receivedMessage);
        } else if (primaryCommand === "admin") {
            adminService.isAdminUser(receivedMessage).then((result) => {
                if (result) {
                    if (arguments[0] === "whois") {
                        botMembersService.whoisDiscordUser(arguments, receivedMessage);
                    } 
                    if (arguments[0] === "guildstats") {
                        userOnlineStatRunner.checkGuildStats(receivedMessage);
                    } 
                    if (arguments[0] === "pending") {
                        if ("remove" === arguments[1]) {
                            appliedExilesServices.removeUser(arguments[2], receivedMessage);
                        } else {
                            appliedExilesServices.pendingInvites(receivedMessage);
                        }
                    }
                    if (arguments[0] === "link") {
                        adminService.forceLinkUser(receivedMessage, arguments);
                    } 
                    if (arguments[0] === "league-eligibility") {
                        adminService.eligibleGuildLeagueParticipants(receivedMessage).then(eligibleMembers => {
                            if (eligibleMembers && eligibleMembers.length > 0) {
                                let msg = `Members eligible for league lucky door prise!\n`
                                eligibleMembers.forEach(e => {
                                    msg += `${e.id} with character [${e.characters[0].name} @ Level ${e.characters[0].level}]\n`
                                });
                                receivedMessage.channel.send(msg);
                            } else {
                                receivedMessage.channel.send(`No eligible members for the door prise, Requires a character at least level ${config.eligiblityLevel}`);
                            }
                        }).catch(err => {
                            console.log(err);
                        });
                    }
                    if (arguments[0] === "league-winner") {
                         adminService.eligibleGuildLeagueParticipants(receivedMessage).then(eligibleMembers => {                           
                            if (eligibleMembers && eligibleMembers.length > 0) {
                                let winningIndex = Math.floor(Math.random() * eligibleMembers.length);
                                let winner = eligibleMembers[winningIndex];
                                receivedMessage.channel.send(`######### DRUM ROLL ##########`);
                                setTimeout(function() {
                                    receivedMessage.channel.send( `And the winner is.... ${winner.id} with character [${winner.characters[0].name} @ Level ${winner.characters[0].level}]`);
                                }, 1000);
                            } else {
                                receivedMessage.channel.send(`There are no eligible members to roll a winner.`);
                            }
                         }).catch(err => {
                            console.log(err);
                        });;
                    }
                    
                } else {
                    receivedMessage.channel.send("You don't have the required access to perform this command.")
                }
            }).catch((err) => {
                Sentry.captureException(err);
            });
        } else if (primaryCommand === "filter") {
            receivedMessage.channel.send(`Latest neversink available @ https://github.com/NeverSinkDev/NeverSink-Filter/releases or if you wish to build your own check out https://www.filterblade.xyz/`);
            // 
        } else {
            receivedMessage.channel.send("I don't understand the command. Try `!help`")
        }
    }
}

function helpCommand(arguments, receivedMessage) {
    let command = arguments[0];
    if (command === 'commands') {
        receivedMessage.channel.send(`Commands: !rank, !patch, !members, !filter, !join, !gladder, !ladder type !help [topic]`);
    } else if (command === 'rank') {
        receivedMessage.channel.send(`!rank - provides the ladder rank in the current league for your account.`);
    } else if (command === 'patch') {
        receivedMessage.channel.send(`!patch - provides a link to the latest patch notes.`);
    } else if (command === 'members') {
        receivedMessage.channel.send(`!members - provides how many members currently in the guild.`);
    } else if (command === 'join') {
        receivedMessage.channel.send(`!join accept <poeAccountName> - accepts the rules and wishes to join the guild see <#518369100867829792>, e.g '!join accept veenarm' - This is the name in your /view/profile/xxxx on the PoE website.`);
    } else if (command === 'ladder') {
        receivedMessage.channel.send(`!ladder - shows the ${config.currentLeague} League Guild Challenge Ladder (Top 10)`);
    } else if (command === 'gladder') {
        receivedMessage.channel.send(`!gladder - shows the ${config.currentLeague} League Guild XP Ladder (Top 10)`);
    } else {
        receivedMessage.channel.send(`I'm not sure what you need help with. Try '!help [topic]' or '!help commands' for a list of commands.`);
    }
}