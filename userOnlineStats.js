const moment = require("moment-timezone");
const MongoClient = require("mongodb").MongoClient;
const dailyOnlineStatsCollection = "dailyOnlineStats";

// GET DATA
// const data = require("./data.json");


module.exports = {

    checkGuildStats: function(receivedMessage) {
        var today = moment().startOf("day");

        MongoClient.connect("mongodb://localhost:27017/igExilesDB", { useNewUrlParser: true }).then(client => {
            dbclient = client.db("igExilesDB");

            // Get the data for today currently.
            let todaySearch = { year: today.year(), month: today.month(), day: today.date() };
            dbclient.collection(dailyOnlineStatsCollection).findOne(todaySearch, function (err, result) {
                if (err) return console.log(err);
                console.log(result);
                if (result) {
                    receivedMessage.author.send(`Greetings Admin.. there have been ${result.users.length} guild members online today.`);
                    receivedMessage.author.send(`${result.users}`);
                } else {
                    receivedMessage.author.send(`Greetings Admin.. seems to be a bug, no result found for today data -should only happen in first 10minutes of a new day, msg me if issues.`);    
                }
            });
        });
    },

    runUserOnlinStatAnalysis: function(data) {
        // This is today as of australia/sydney
        var today = moment().startOf("day");
        console.log(`## Starting Online Stats Runner with ${data ? data.length : 0} records to check ##`);

        MongoClient.connect("mongodb://localhost:27017/igExilesDB", { useNewUrlParser: true }).then(client => {
            dbclient = client.db("igExilesDB");

            let seenOnlineUsers = [];

            // Find all users who've had a last_online as today, add them to the users seen list.
            data.forEach(e => {
                if (e.last_online) {
                    let lastDayOnlineLocal = moment.tz(e.last_online, 'Australia/Sydney').startOf("day");
                    if (today.isSame(lastDayOnlineLocal)) {
                        seenOnlineUsers.push(e.id);
                    }
                }
            });

            // Get the data for today currently.
            let todaySearch = { year: today.year(), month: today.month(), day: today.date() };
            dbclient.collection(dailyOnlineStatsCollection).findOne(todaySearch, function (err, result) {
                if (err) return console.log(err);

                // IF RESULT, WE HAVE DATA ALREADY
                if (result) {
                    const mergedUsers = Array.from(new Set(seenOnlineUsers.concat(result.users)));

                    // We only want to update if there is a length change in the merged array.
                    // We never remove users from the lists, so it can be assumed if it's the same length, no changes.
                    if (mergedUsers.length > result.users.length) {
                        dbclient.collection(dailyOnlineStatsCollection).updateOne(todaySearch, { $set: { users: mergedUsers, guildMembers : data.length } }, function (err, res) {
                            if (err) return console.log(err);
                            console.log(`## Online Stat Runner Complete ##`);
                        });
                    }
                } else {
                    console.log("Today didn't exist in the onlineStats collection, creating it, and inserting latest data.");

                    let todayDataObject = todaySearch;
                    todayDataObject.users = seenOnlineUsers;

                    dbclient.collection(dailyOnlineStatsCollection).insertOne(todayDataObject, function (err, result) {
                        if (err) return console.log(err);
                        if (result ? console.log("User stats created for today") : console.log("Users data not saved for today"));
                        console.log(`## Online Stat Runner Complete ##`);
                    });
                }
            });
        })
            .catch(function (err) {
                console.log(err);
            });
    }
}