# OceanicExiles-BOT

This is a Discord bot for the Path of Exile Guild - [Oceanic Exiles](https://www.pathofexile.com/guild/profile/7022)
The bot is the meet and greet, status maintainer in discord.

## BOT Performs:
- A linking between discord Uid to poe account name
- Includes wikibot for poe with caching added
- Tails a client.txt file and outputs from your in game account to a discord channel (TODO split out into a new bot)
- Compares and maintaines discord user roles member/ex-member based on if they have been removed from the guild.
- Also now maintaines 'Officer' roles.
- Syncs guild members last_online status and maintains advice on non-active members
- Creates daily stats in a new collection for last_online
- Has a ladder feature to check a guild members current ladder possition (currently guild league, but will be modified to each primary league)
- Automates the joining process from new discord users
- Saves users characters and there ranks.

## pre-reqs
- nodejs (required)
- npm (required)
- valid approved bot user and bot authentication token (required)
- valid POESESSID from a valid guild member (required) 
- POE Client running to generate guid-log texts (optional)


## Required Settings
- config.json :
    - you'll need your own "token" which is the BOT login token from discord.
    - enableGuildChatLogging - can only work on a machine that's running Path of exile client, and generating client.txt logs (user must be logged in as a guild member).
    - poeClientTxtLocation - If chat logging is enabled, the location to your client.txt file
- authenticate.js :
    - POESESSId to be added to cookieJar