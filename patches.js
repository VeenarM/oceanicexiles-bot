const request = require('request');
const cheerio = require('cheerio');
const Sentry = require('@sentry/node');

module.exports = {

    getLatestPatch: function (receivedMessage) {
        request('https://www.pathofexile.com/forum/view-forum/patch-notes', { json: true }, (err, res, body) => {
            if (err) { return console.log(err) }

            let $ = cheerio.load(body)
            var title
            var url = 'https://www.pathofexile.com'

            $('table.forumTable tbody').filter(function () {
                var data = $(this)
                var first = data.children().first()
                var thread = first.find('td .title a')
                title = thread.text().trim()
                url += thread.attr('href')
            })

            receivedMessage.channel.send("Latest POE Patch Version is : " + title)
            receivedMessage.channel.send(url)
        });
    }

};