const Discord = require("discord.js");
const puppeteer = require('puppeteer');
const wikiRegex = new RegExp("\\[\\[([^\\[\\]]*)\\]\\]", "gu");
const urlRegex = new RegExp("\\w", "g");
const cacheSettings = require('./cache-config.json');
var CachemanMongo = require('cacheman-mongo');
var cache = new CachemanMongo(cacheSettings.serverOptions);
const config = require("./config.json");

const SimpleNodeLogger = require('simple-node-logger')

errorLog = SimpleNodeLogger.createSimpleLogger({
    logFilePath: './logs/error.log',
    timestampFormat: 'YYYY-MM-DD HH:mm:ss'
});

log = SimpleNodeLogger.createSimpleLogger({
    logFilePath: './logs/requests.log',
    timestampFormat: 'YYYY-MM-DD HH:mm:ss'
});

errorLog.setLevel('error');

var botAuthconfig = require("./bot-auth.json");

var client = new Discord.Client({
    disableEveryone: true,
    disabledEvents: ["TYPING_START"]
});

var browswer;

client.token = botAuthconfig.discordToken;

client.login();
console.log("Logged in");

//Setup our browswer
(async () => {
    browser = await puppeteer.launch({
        ignoreHTTPSError: true,
        headless: true,
        handleSIGHUP: true,
        args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage']
    });
})();


client.on("message", (message) => {
    if (message.author.id == client.user.id || message.content.startsWith("!")) {
        //Disabled this check, as it causes it to not do a check if you post twice in a row.
        //return;
    }

    try {
        let matches = wikiRegex.exec(message.cleanContent);
        if (matches != null && matches.length > 0) {
            for (let i = 1; i < matches.length; i++) {
                handleItem(titleCase(matches[i]), message.channel);
            }
        }
    } catch (error) {
        errorLog.error(`"${error.message}"`);
    }
});

async function handleItem(name, channel) {
    let itemUrlPart = convertToUrlString(name);
    var url = config.wikiOpts.wikiURL + itemUrlPart;
    let initalMessage = "Retrieving details from the Wiki for **" + name + "**";
    var messageId;
    await channel
        .send(initalMessage)
        .then(message => {
            //console.log(`Sent message: ${message.id} ${initialMessage}`);
            messageId = message.id;
        }).catch(error => {
            errorLog.error(`"${error.message}" "${name}"`);
        })

    if (messageId != null) {
        cache.get(name, function (err, value) {
            if (err) {
                errorLog.error(`"${err.message}" "${name}"`);
            }

            if (value != null) {
                if (value.itemFound === false) {
                    channel.fetchMessage(messageId).then(message => {
                        message.edit("Could not get details from the Wiki for **" + name + "** (Cached Result)");
                    });
                } else {
                    // Cached Result is successful and has a valid reply.
                    if (value.screenshot) {
                        channel.send(value.output + ' (Cached Result)', { file: value.screenshot.buffer });
                    } else {
                        // No screenshot
                        channel.fetchMessage(messageId).then(message => {
                            message.edit(value.output + ' (Cached Result)');
                        });
                    }
                }
            } else {
                // TODO make this aysnc (inside cache.get() not async?)
                getImage(url)
                    .then((result) => {
                        log.info(`"${name}" "${url}"`);
                         //need a way that lets us add an attachment message, currently I can only edit text to it

                        if (result.success == false) {
                            channel
                                .fetchMessage(messageId)
                                .then(message => {
                                    message.edit("Could not get details from the Wiki for **" + name + "**");
                                    // Cache the fact we attempted for this item, so we don't spam URL requests
                                    cache.set(name, { itemFound: false, screenshot: false }, cacheSettings.ttl, function (err, value) {
                                        if (err) {
                                            errorLog.error(`"${err.message}" ${name}"`);
                                        }
                                    });
                                })
                                .catch(error => {
                                    errorLog.error(`"${error.message}" "${name}"`);
                                })
                        } else {
                            let output = '<' + url + '>';
                            //if no screenshot, just edit the original message
                            if (result.screenshot == false) {
                                channel
                                    .fetchMessage(messageId)
                                    .then(message => {
                                        message.edit(output);
                                        console.log('caching item - ' + name);

                                        cache.set(name, { itemFound: true, screenshot: false, output: output }, cacheSettings.ttl, function (err, value) {
                                            if (err) {
                                                errorLog.error(`"${err.message}" "${name}"`);
                                            }
                                        });

                                    })
                                    .catch(error => {
                                        errorLog.error(`"Could not edit message ${messageId}" "${name}"`);
                                    })
                            } else {
                                //otherwise delete the message and create a new one with the screenshot
                                channel
                                    .fetchMessage(messageId)
                                    .then(message => {
                                        message.delete();
                                    })
                                    .catch(error => {
                                        errorLog.error(`"Could not delete message ${messageId}" "${name}"`);
                                    })
                                channel.send(output, { file: result.screenshot });

                                cache.set(name, { itemFound: true, screenshot: result.screenshot, output: output }, cacheSettings.ttl, function (err, value) {
                                    if (err) {
                                        errorLog.error(`"${err.message}" "${name}"`);
                                    }
                                });
                            }
                            console.log('Found in the wiki and sent: ' + url);
                        }
                    })
                    .catch(error => {
                        errorLog.error(`"${error.message}" "${name}"`);
                    })

            }
        });

    }
}

async function getImage(url) {
    //console.time('getPage')
    const page = await browser.newPage();
    //Disabling Javascript adds 100% increased performance
    await page.setJavaScriptEnabled(config.wikiOpts.enableJavascript)
    var output = {
        screenshot: false,
        success: false
    }

    //Set a tall page so the image isn't covered by popups
    await page.setViewport({ 'width': config.wikiOpts.width, 'height': config.wikiOpts.height });

    try {
        //played around with a few different waitUntils.  This one seemed the quickest.
        //If you don't disable Javascript on the PoE Wiki site, removing this parameter makes it hang
        await page.goto(url, { waitUntil: 'load' });
    } catch (error) {
        errorLog.error(`"${error.message}" "${url}"`);
    }

    var invalidPage = await page.$(config.wikiOpts.wikiInvalidPage);
    //if we have a invalid page, lets exit
    if (invalidPage != null) {
        return output;
    }

    var infoBox = await page.$('.infocard');
    if (infoBox != null) {
        try {
            output.screenshot = await infoBox.screenshot();
            output.success = true;
        } catch (error) {
            output.success = true;
        }
        return output;
    }

    //if we have a div for the item, screenshot it.
    //If not, just return the page without the screenshot
    const div = await page.$(config.wikiOpts.wikiDiv);
    if (div != null) {
        try {
            output.screenshot = await div.screenshot();
            output.success = true;
        } catch (error) {
            output.success = true;
        }
    } else {
        output.success = true;
    }

    await page.close();
    //console.timeEnd('getPage')
    return output;

}

function convertToUrlString(name) {
    return name.replace(new RegExp(" ", "g"), "_");
}

function titleCase(str) {
    var str = str.trim();
    let excludedWords = ["of", "and", "the", "to", "at", "for"];
    let words = str.split(" ");
    for (var i in words) {
        if ((i == 0) || !(excludedWords.includes(words[i].toLowerCase()))) {
            words[i] = words[i][0].toUpperCase() + words[i].slice(1, words[i].length);
        } else {
            continue;
        }
    }
    return words.join(" ");
};