const request = require('request');
const accNameRegex = new RegExp(/^[a-zA-Z0-9_]+$/);
const MongoClient = require('mongodb').MongoClient;
var config = require("./config.json");
const igExilesCollection = "igExiles";
const SORT = require('fast-sort');
const Sentry = require('@sentry/node');
const escape = require('markdown-escape');

module.exports = {

    checkPoeLeague: function (args, receivedMessage) {
        if (args[0] == 'help') {
            receivedMessage.channel.send("Usage: !rank");
            return;
        }

        MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
            dbclient = client.db(config.exileDbName);
            dbclient.collection(igExilesCollection).findOne({ discordUid: receivedMessage.author.id }, function (err, result) {
                if (err) return console.log(err);

                if (!result) {
                    receivedMessage.channel.send(`!rank is not available for users who've not linked there discord accounts to there IGN accounts, please see #rules-recruitment`);
                } else {

                    // Output Guild Rank - Then check POE API Rank.
                    var guildPromise = new Promise(function(resolve, error) {
                        
                       // TODO maybe update the ranks now?
                    
                        dbclient.collection(igExilesCollection).find({ discordUid: { $exists: true }, characters : { $gt: [] }}).toArray(function (err, result) {
                            if (err) return console.log(err);                           
                            let betrayalCharacterList = result.filter(o => o.characters.find(f => f.league === config.currentLeague));

                            // Sort the characters first (filter out non-league chars.)
                            betrayalCharacterList.forEach(c => {
                                c.characters = c.characters.filter(c => c.league === config.currentLeague);
                                SORT(c.characters).by([
                                    { desc : 'level'},
                                    { desc : 'experience'},
                                    { asc :  'hundredReachedAt' }
                                ]);
                            })

                            // Now sort the exiles by there highest character with earlier achieved hundred date.
                            SORT(betrayalCharacterList).by([
                                { desc: c => c.characters[0].level },
                                { desc: c => c.characters[0].experience },
                                { asc: c => c.characters[0].hundredReachedAt },
                            ]);

                            var exile;
                            var rank = 0;
                            for (var i = 0; i < betrayalCharacterList.length; i++) {
                                if (betrayalCharacterList[i].discordUid == receivedMessage.author.id) {
                                    exile = betrayalCharacterList[i];
                                    rank = i + 1;
                                    break;
                                }
                            }

                            var guildString = '';
                            if (exile && exile.characters) {
                                var exileHighestChar = exile.characters[0];
                                guildString = `[Guild] Rank.${rank} Character.${exileHighestChar.name} Level.${exileHighestChar.level} Class.${exileHighestChar.class} Experience.${exileHighestChar.experience}\n`;
                            } else {
                                guildString = `[Guild] - No characters loaded for member yet, may need to wait for next refresh.\n`;
                            }
                            resolve(guildString);
                        });
                    });

                    guildPromise.then(function(guildString) {
                        // If here, user was located - attempt to locate user using the API.
                        request(`https://api.pathofexile.com/ladders/${config.currentLeague}?accountName=${result.id}`, { json: true }, (err, res, body) => {
                            if (err) { return console.log(err); }

                            if (!body || !body.entries) {
                                receivedMessage.channel.send(`\`\`\`css\n${guildString}[Global] There seems to be a problem connecting to api.pathofexile.com [Global] ranking unable to be retrieved.\`\`\``);
                                return;
                            }

                            if (body.entries.length == 0) {
                                receivedMessage.channel.send(`\`\`\`css\n${guildString}[Global] ${receivedMessage.author.username} currently has no rank in ${config.currentLeague}. Note: The PoE API Only shows the top 15,000 users.\`\`\``);
                                return;
                            }

                            if (body.entries.length == 1) {
                                let e = body.entries[0]
                                receivedMessage.channel.send(`\`\`\`css\n${guildString}[Global] Rank.${e.rank} Character.${e.character.name} Level.${e.character.level} Class.${e.character.class} Experience.${e.character.experience}\n\`\`\``)
                            } else {
                                var msgL = `\`\`\`css\n${guildString}[Multiple Globally Ranked Characters for ${receivedMessage.author}]\n`
                                body.entries.forEach(e => {
                                    msgL += `[Global] Rank.${e.rank} Character.${e.character.name} Level.${e.character.level} Class.${e.character.class} Experience.${e.character.experience}\n`;
                                });
                                msgL += '```';
                                receivedMessage.channel.send(msgL);
                            }
                        });
                    }).catch(function() {
                        console.log(`Exception in updating/retrieving guild rank`)
                    });
               }

            });
        }).catch(function (err) {
            console.log(err)
        });

    },
    printGuildLadder(receivedMessage) {
        request(`https://api.pathofexile.com/ladders/${config.guildLeague}`, { json: true }, (err, res, body) => {
            if (err) console.log(err);
            if (body && body.entries) {
                let msg = '';
                body.entries.forEach(e => {
                    if (msg.length < 900) {
                        msg += `Rank ${e.rank}) ${escape(e.account.name)}, Character: ${escape(e.character.name)}(${e.character.level})[${e.character.experience}]\n`;
                    }

                });
                
                receivedMessage.channel.send({
                    embed: {
                        "color": 12488439,
                        "fields": [
                            {
                                "name": `${config.guildLeague} Private League Rankings`,
                                "value": msg,
                            }
                        ]
                    }
                });
            } else {
                receivedMessage.channel.send(`API returned no data.`);
            }
        });
    }
};