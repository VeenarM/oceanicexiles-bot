const index = require('./index');
const MongoClient = require('mongodb').MongoClient;
const igExilesCollection = "igExiles";
const appliedMembersCollection = "appliedExiles";
const memberCheck = require("./members-check");
const escape = require('markdown-escape');
const config = require("./config.json");
const authenticate = require("./authenticate");
const request = authenticate.request;
const Sentry = require('@sentry/node');
const appliedRole = '521284023352557568';
const unverifiedRole = '519771288345247744';
const exMemberRole = '419998937601146880';
const memberRole = '125822121468362752';

module.exports = {
    appliedRole,
    unverifiedRole,
    exMemberRole,
    memberRole,

    joinAcceptGuildRules: function (args, receivedMessage) {
        if (args[0] !== 'accept' && args[0] !== 'Accept') {
            receivedMessage.author.send("Rules have not been accepted, no further action will be taken.");
            return;
        }

        let accountName = args[1];
        
        console.log(`${receivedMessage.author.username} has accepted the terms and conditions applied with accountName ${accountName} at ${new Date()}`);

        if (!accountName) {
            receivedMessage.author.send("No PoE account name provided.");
            return;
        }

        if (accountName.length < 3 || accountName.length > 31) {
            receivedMessage.author.send("AccountName provided is invalid, doesn't meet the length requirements of PoE account name conventions.");
            return;
        }

        // Not sanitizing, as we encode the input to a URL - if the URL fails to provide what we want, we reject anyway.
        var validateAccountName = new Promise(function(resolve, errror) {
            let url = `https://www.pathofexile.com/account/view-profile/${accountName}/characters`
            let encodedUrl = encodeURI(url); //Need to encode for those whom have funny characters...

            request({ url : encodedUrl, json : true}, (err, res, body) => {
                if (err) {
                    console.log(`Error when looping character info for id ${accountName}`);
                    resolve(buildResponse(0003, `Error attempting to validate user profile, poe website may be unavailable, please try again later`, null));
                } else {
                    try {
                        // 0000 - success, 0001 - not found, 0002 - private profile, 0003 error 
                        let profileNotFound = body.indexOf('Profile Not Found') !== -1;
                        let profileIsPrivate = body.indexOf('This profile tab has been set to private') !== -1;
                        if (profileNotFound) {
                            resolve(buildResponse(0001, `Profile Not Found - Please verify your PoE account name this is on the PoE website /account/view-profile/accountNameHere`, null));
                        } else if (profileIsPrivate) {
                            resolve(buildResponse(0002, `Profile Found - However is private.`, null));
                        } else { // Profile is found and not private.
                            let foundIdx = body.indexOf('new C({"name"');
                            if (foundIdx === -1) {
                                throw new Error(`Object for Name not found in body response object`);
                            } else {
                                let str = body.substr(foundIdx + 6);
                                let fullJsonString = str.substr(0, str.indexOf('}') + 1);
                                let characterObject = JSON.parse(fullJsonString);
                                resolve(buildResponse(0000, 'Profile Found - Character Retrieved', characterObject));
                            }
                        }
                    } catch (e1) {
                        console.log(`${accountName} - with URL(${encodedUrl}) - ${e1}`);
                        resolve(buildResponse(0002, `Profile Found - Character tab unavailable to retrieve character data`, null));
                    }
                }
            });
        })

        validateAccountName.then(function(response) {
            // Profile not found, or Error in retrieving profile...
            if (response.code === 0001 || response.code === 0003) {
                receivedMessage.author.send(`${response.message}`);
            } else { // Profile has been retrieved successfully
                doLinkChecking(receivedMessage, accountName, response.character);
            }
        }).catch(function(err) { 
            console.log(`Error in validateAccountName URL Call - ${err}`);
        });
    }
}

function buildResponse(c, m, char) {
    var response = {
        code: c,
        message: m,
        character: char
    };
    return response;
}

function doLinkChecking(receivedMessage, accountName, character) {
    MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
        dbclient = client.db(config.exileDbName);

        // CHECK IF USER IS ALREADY IN THE TEMP COLLECTION (APPLIED/ACCEPTED). // TODO unlikely needed after implmentation of the 'HTTP URL Checker'
        dbclient.collection(appliedMembersCollection).findOne({ discordUid: receivedMessage.author.id }, function (err, result) {
            if (err) return console.log(`error finding user in temp store - ${receivedMessage.author.id} - ${err}`);

            if (result) {
                receivedMessage.author.send(`Your application to join Oceanic Exiles already exists for PoE Account Name **IGN:** ${escape(accountName)}`);
            } else {
                // CHECK IF USER IS ALREADY A MEMBER
                // CHECK IF USER IS LINKED ALREADY BY DISCORD ID
                dbclient.collection(igExilesCollection).findOne({ discordUid: receivedMessage.author.id }, function (err, res) {
                    if (err) return console.log(`Error finding current guild member with discordUid: ${receivedMessage.author.id}  ${err}`);
                    if (res) {
                        receivedMessage.author.send(`Your account is already linked with the bot.`);
                        return;
                    }

                    // CHECK IF USER IS ALREADY A MEMBER (BY ACCT NAME, AND NOT LINKED -> MODIFY ROLES)
                    dbclient.collection(igExilesCollection).findOne({ "id": { $regex: new RegExp(accountName, "i") } }, function (err, res) {
                        if (err) return console.log(`Error finding current guild member with IGN: ${accountName}  ${err}`);

                        // MEMBER FOUND -> LINK THE ACCOUNT
                        if (res && !res.linkDate) {
                            var newvalues = { $set: { "discordUid": receivedMessage.author.id, "discordUsername": receivedMessage.author.username, "linkDate": new Date() } };
                            dbclient.collection(igExilesCollection).updateOne({ "id": res.id }, newvalues, function (err, exileRes) {
                                if (err) return console.log(`Error linking account ${accountName} ${err}`);

                                // MODIFY THE ROLES
                                let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);
                                discordGuild.fetchMember(receivedMessage.author.id).then(guildMember => {
                                    guildMember.addRole(memberRole).then(gm => {
                                        guildMember.removeRoles([exMemberRole, unverifiedRole, appliedRole]).catch(function (err) {
                                            console.log(`Discord User (${discordUid}) - Failed to remove roles in addNewMember roles ${err}`)
                                        });
                                    }).catch(function (err) {
                                        console.log(`Discord User (${discordUid}) - Failed to add role member ${err}`)
                                    });
                                }).catch(err => {
                                    console.log(err)
                                });

                                receivedMessage.author.send(`You're already a current guild member in game, created a link from PoE -> Discord and updated your user roles.`);

                                dbclient.collection(appliedMembersCollection).deleteOne({ "ignAccountName": { $regex: new RegExp(accountName, "i") } }, function(err, result) {
                                    if (err) return console.log(`Error deleting linked account from applied collection ${accountName} ${err}`);
                                    console.log(`${accountName} document from ${appliedMembersCollection} deleted (may not of already existed)`);
                                });
                            });
                        } else {
                            registerNewMemberRequest(dbclient, receivedMessage, accountName, character);
                        }
                    });
                });
            }
        });

    }).catch(function (err) {
        console.log(err)
    });
}


// No existing members found - time to create a entry for guild recruitment pending member invite.
function registerNewMemberRequest(dbclient, receivedMessage, accountName, character) {
    var discordUser = { discordUid: receivedMessage.author.id, discordUsername: receivedMessage.author.username, ignAccountName: accountName, latestCharacter: character, applyDt: new Date() };
    dbclient.collection(appliedMembersCollection).insertOne(discordUser, function (err, res) {
        if (err) return console.log(`Error adding account to pending members collection`);

        // MODIFY THE ROLES
        let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);
        discordGuild.fetchMember(receivedMessage.author.id).then(guildMember => {
            guildMember.addRole(appliedRole).catch(err => {
                console.log(err)
            });
        }).catch(err => {
            console.log(err)
        });

        let msg = `@here - Discord Author: ${receivedMessage.author} has requested to join the guild and has read/accepted the guild rules.\n`;
        
        if (memberCheck.currentMemberLength >= 250) {
            msg += `The user has been advised that we're currently full and to try again later.`;
            index.dclient.channels.get(config.membersLogChannel).send(msg);
            receivedMessage.author.send(`Thankyou for your application to Oceanic Exiles. Unfortunately we're currently full, your application has been saved and if positions open we'll contact you. Feel free to chat in public channels.`);
        } else {
            if (character) {
                msg += `Please invite to guild - **PoE Account:** ${escape(accountName)}, **IGN:** ${character.name}`;
            } else {
                msg += `Please invite to guild - **PoE Account:** ${escape(accountName)}, Private profile, need to contact requestor for their **IGN**`;
            }
            index.dclient.channels.get(config.membersLogChannel).send(msg);
            receivedMessage.author.send(`Thankyou for your application to Oceanic Exiles. If your character profile is public a guild officer will invite you shortly, otherwise you'll be contacted in discord to get your in game character name for an invite.`);
        }
    });
}