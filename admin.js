const index = require('./index');
const config = require("./config.json");
const newMember = require("./new-member");
const botAdminRole = '538276246388473857';
const Sentry = require('@sentry/node');
const MongoClient = require('mongodb').MongoClient;
const SORT = require('fast-sort');

module.exports = {
    // We do it this way to allow private messaging, doing receivedMessage.member.roles doesn't work if it's a DM.
    isAdminUser: function (receivedMessage) {
        return new Promise(function (resolve, error) {
            let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);
            discordGuild.fetchMember(receivedMessage.author.id).then(guildMember => {
                return resolve(guildMember.roles.has(botAdminRole));
            }).catch(function (err) {
                console.log(`Defaulting admin to false, due to error fetching guild member ${receivedMessage.author}  - ${err}`);
                return resolve(false);
            });
        });
    },
    forceLinkUser: function (receivedMessage, args) {

        let authorId = args[1];
        let toIgn = args[2];

        if (authorId && toIgn) {
            dbclient.collection(config.igExilesCollection).findOne({
                "id": {
                    $regex: new RegExp(toIgn, "i")
                }
            }, function (err, res) {
                if (err) return console.log(`Error finding current guild member with IGN: ${toIgn}  - ${err}`);

                // MEMBER FOUND -> LINK THE ACCOUNT
                if (res && !res.linkDate) {
                    // MODIFY THE MEMBERS ROLES
                    let discordGuild = index.dclient.guilds.get(config.oceanicExilesGuildId);

                    discordGuild.fetchMember(authorId).then(guildMember => {

                        var newvalues = {
                            $set: {
                                "discordUid": authorId,
                                "discordUsername": guildMember.user.username,
                                "linkDate": new Date()
                            }
                        };

                        dbclient.collection(config.igExilesCollection).updateOne({
                            "id": res.id
                        }, newvalues, function (err, exileRes) {
                            if (err) return console.log(`Error linking account ${toIgn} ${err}`);

                            guildMember.addRole(newMember.memberRole).then(gm => {
                                gm.removeRoles([newMember.exMemberRole, newMember.unverifiedRole, newMember.appliedRole]).catch(err => {
                                    console.log(`Error removing roles for member ${guildMember} - ${err}`)
                                });
                            }).catch(err => {
                                console.log(`Error adding member role for member ${guildMember} - ${err}`)
                            });

                            receivedMessage.channel.send(`Successfully linked ${authorId} to ${toIgn}`);

                        })
                    });
                } else {
                    receivedMessage.channel.send(`${toIgn} either not found, or found but already linked - check your request.`);
                }
            });

        }
    },
    eligibleGuildLeagueParticipants : function (receivedMessage) {
        return new Promise(function (resolve, error) {
            MongoClient.connect(config.exileDbUrl, { useNewUrlParser: true }).then(client => {
                client.db(config.exileDbName).collection(config.igExilesCollection).find({ discordUid: { $exists: true }, characters : { $gt: [] }}).toArray(function (err, result) {
                    if (err) return console.log(err);
        
                    let leagueCharacters = result.filter(o => o.characters.find(f => f.league === config.guildLeague));
        
                    // Sort the characters first (filter out non-league chars.)
                    leagueCharacters.forEach(c => {
                        c.characters = c.characters.filter(c => c.league === config.guildLeague);
                        SORT(c.characters).by([
                            { desc : 'level'},
                            { desc : 'experience'},
                            { asc :  'hundredReachedAt' }
                        ]);
                    })
        
                    // Now sort the exiles by there highest character with earlier achieved hundred date.
                    SORT(leagueCharacters).by([
                        { desc: c => c.characters[0].level },
                        { desc: c => c.characters[0].experience },
                        { asc: c => c.characters[0].hundredReachedAt },
                    ]);
                    
                    let eligibleMembers = leagueCharacters.filter(f => f.characters[0].level >= config.eligiblityLevel);
                    return resolve(eligibleMembers);
                });
            }).catch(err => {
                console.log(err);
                return resolve(0);
            });
        });
    }
    
}


