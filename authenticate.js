const R = require('request');
const Sentry = require('@sentry/node');
const cookieJar = R.jar();
const botAuthConfig = require('./bot-auth.json');
const cookie = R.cookie(`POESESSID=${botAuthConfig.POESESSID}`);
const url = 'https://www.pathofexile.com';
cookieJar.setCookie(cookie, url);
const request = R.defaults({ jar: cookieJar });
console.log('Authenticated Cookie Jar Configured.');

module.exports = {
    request: request
}






