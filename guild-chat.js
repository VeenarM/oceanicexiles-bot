const Tail = require('always-tail');
const index = require('./index');
const config = require('./config.json');
const Sentry = require('@sentry/node');
var tail = new Tail(config.poeClientTxtLocation, '\n');

const AUSSIE_GUILD_TXT = '&<AUSSIE>';
const GUILD_MEMBER_TXT = '] Guild member';

tail.on("line", function (data) {
    var writeLine = data.indexOf(AUSSIE_GUILD_TXT);
    var writeFromLength = 10; // Writes text with out &<AUSSIE>

    if (writeLine === -1) {
        writeLine = data.indexOf(GUILD_MEMBER_TXT);
        writeFromLength = 2;
    }
    
    if (writeLine != -1) {
        let dateTime = data.substr(0,20);
        let nameAndMessage = data.substr(writeLine + writeFromLength); 

        // Bold this message in the output font.
        if (writeFromLength === 2) {
            nameAndMessage = nameAndMessage.replace(/^/, '**');
            nameAndMessage += '**';
        }

        let channel = index.dclient.channels.get(config.guildChatChannelId);
        if (channel) {
            channel.send(dateTime + nameAndMessage);
        }        
    }
});

tail.on("error", function (error) {
    console.log('ERROR: ', error)
});

tail.watch()
